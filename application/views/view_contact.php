<div class="banner-slider" style="background-image: url(<?php echo base_url(); ?>public/uploads/<?php echo $setting['banner_contact']; ?>)">
    <div class="bg"></div>
    <div class="bannder-table">
        <div class="banner-text">
            <h1><?php echo $this->security->xss_clean($page['contact_heading']); ?></h1>
        </div>
    </div>
</div>

<div class="contact-area bg-area pt_50 pb_80">
    <div class="container wow fadeIn">
        <div class="row">
            <div class="col-md-7 mt_30 wow fadeIn" data-wow-delay="0.1s">
                <div class="contact-form">
                    <div class="headstyle">
                        <h4>Contact Form</h4>
                    </div>
                    <?php
                    if($this->session->flashdata('error')) {
                        echo '<div class="error-class">'.$this->session->flashdata('error').'</div>';
                    }
                    if($this->session->flashdata('success')) {
                        echo '<div class="success-class">'.$this->session->flashdata('success').'</div>';
                    }
                    ?>
                    <?php echo form_open(base_url().'contact/send_email',array('class' => '')); ?>
                        <div class="form-row">

                            <div class="form-group">
								<label>Name:*</label>
                               	<input type="text" class="form-control" placeholder="Example: xyz***" name="name">
                            </div>
                            <div class="form-group">
                            	<label>Phone Number:*</label>
                                <input type="text" class="form-control" placeholder="Example: 019***" name="phone">
                            </div>
                            <div class="form-group">
                            	<label>Email Address:*</label>
                                <input type="text" class="form-control" placeholder="Example: xyz@gmail.com" name="email">
                            </div>
                            <div class="form-group">
                            	<label>Message</label>
                                <textarea class="form-control" placeholder="Message body" name="message" style="height:195px;"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary" name="form_contact">Submit</button>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="col-md-5 mt_30 wow fadeIn" data-wow-delay="0.1s">
                <div class="headstyle">
                    <h4>Contact Information</h4>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.2s">
                        <div class="contact-item mb_30">
                            <div class="contact-text">
                                <h3>Address</h3>
                                <p>
                                    <?php echo $this->security->xss_clean(nl2br($page['contact_address'])); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.3s">
                        <div class="contact-item mb_30">
                            <div class="contact-text">
                                <h3>Email</h3>
                                <p>
                                    <?php echo $this->security->xss_clean(nl2br($page['contact_email'])); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.4s">
                        <div class="contact-item mb_30">
                            <div class="contact-text">
                                <h3>Phone</h3>
                                <p>
                                    <?php echo $this->security->xss_clean(nl2br($page['contact_phone'])); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt_50 wow fadeIn">
                <div class="map-area">
                    <div class="headstyle">
                        <h4>Map Location</h4>
                    </div>
                    <?php echo $this->security->xss_clean($page['contact_map']); ?>
                </div>
            </div>
        </div>
    </div>
</div>